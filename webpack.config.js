var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  target: "web",
  port: 8080,
  devtool: 'cheap-source-map',
  entry: {
    vendor: [
      "./node_modules/angular/angular.min.js",
      "./node_modules/angular-aria/angular-aria.min.js",
      "./node_modules/angular-animate/angular-animate.min.js",

      "./node_modules/angular-material/angular-material.css",
      "./node_modules/angular-material/angular-material.layouts.css"
    ],
    app: "./src/scripts/app.js",
    testData: "./testData.js"
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "scripts/[name].js"
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/html/index.html',
      inject: 'head'
    }),
    new ExtractTextPlugin("styles/[name].css")
  ],
  externals: [
    "angular",
    "__TEST_DATA__"
  ],
  module: {

    noParse: [
      /[\/\\]node_modules[\/\\]angular(-material|-aria|-animate)?[\/\\]angular(-material|-aria|-animate)?\.min\.js$/,
      "testData.js"
    ],
    loaders: [
      {
        test: /\.html$/,
        loader: 'html'
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: path.join(__dirname, 'src/scripts'),
        exclude: /node_modules/,
        query: {
          presets: ["es2015", "stage-0"]
        }
      }
    ]
  }
};
