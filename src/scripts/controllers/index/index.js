import StoreItem from '../../store/StoreItem';

function indexController($scope, Store, Cache) {
  const dataObject = {
    storeData: Store.getData(),
    cacheData: Cache.getData(),
  };

  let selectedItem = null;
  let selectedCacheItem = null;

  Cache.setUpdateCallback(() => {
    dataObject.cacheData = Cache.getData();
  });

  Store.setUpdateCallback(() => {
    dataObject.storeData = Store.getData();
  });

  function unSelect() {
    selectedItem = null;
    selectedCacheItem = null;
  }

  function selectCacheItem(itemId) {
    selectedCacheItem = Cache.getItemById(itemId);
  }

  function selectItem(itemId) {
    selectedItem = Store.getItemById(itemId).clone(); // to avoid update original tree by link
  }

  function reset() {
    Store.reset();
    Cache.reset();
  }

  function commit() {
    const updatedCacheItems = Store.update(Cache.getData());
    Cache.update(updatedCacheItems);
    unSelect();
  }

  function addToCache() {
    if (!selectedItem) {
      return;
    }

    Cache.add(selectedItem);
    unSelect();
  }

  function remove() {
    if (!selectedCacheItem) {
      return;
    }

    Cache.deleteItem(selectedCacheItem.id);
    unSelect();
  }


  function create() {
    if (!selectedCacheItem) {
      return;
    }

    const item = new StoreItem();

    item.setParent(selectedCacheItem.id);
    item.setValue(prompt('Enter label', ''));

    Cache.createItem(item);

    unSelect();
  }

  function edit() {
    if (!selectedCacheItem) {
      return;
    }

    const dialog = prompt('Enter new label', selectedCacheItem.value);

    if (dialog) {
      selectedCacheItem.setValue(dialog);
    }

    Cache.updateItem(selectedCacheItem);
    unSelect();
  }

  angular.extend($scope,
    { dataObject,
    selectedItem,
    selectedCacheItem,

    selectItem,
    selectCacheItem,
    reset,
    commit,
    addToCache,
    remove,
    create,
    edit }
  );

  reset();
}

export default ['IndexController', indexController];
