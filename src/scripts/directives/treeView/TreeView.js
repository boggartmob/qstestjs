function treeView() {
  return {
    restrict: 'A',
    scope: {
      items: '=',
      onSelect: '=',
    },
    template: require('./treeView.html'),
    link: (scope) => {
      let children = {};
      let roots = {};
      const selectedId = { id: null };

      function prepareTreeData(rawData) {
        const _children = {};
        const _roots = [];

        Object.keys(rawData).map((key) => {
          const item = rawData[key];

          if (!rawData[item.parentId]) {
            _roots.push(item.id);
          }

          if (!_children[item.parentId]) {
            _children[item.parentId] = [];
          }

          if (rawData[item.parentId]) {
            if (_children[item.parentId].indexOf(item.id) === -1) {
              _children[item.parentId].push(item.id);
            }
          }
        });

        return { children: _children, roots: _roots };
      }

      function update(data) {
        const treeData = prepareTreeData(data);

        children = treeData.children;
        roots = treeData.roots;

        angular.extend(scope,
          { children,
            roots,
            selectedId }
        );
      }

      scope.$watchCollection('items', update);
    },
  };
}

export default ['treeView', treeView];
