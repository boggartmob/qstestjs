function treeItem(RecursionHelper) {
  return {
    restrict: 'A',
    template: require('./treeItem.html'),
    scope: {
      id: '=',
      items: '=',
      children: '=',
      onSelect: '=',
      selectedId: '=',
    },

    compile: (element) => {
      return RecursionHelper.compile(element, {
        pre: (scope) => {
          const isSelected = (item) => {
            return item && (scope.selectedId.id === item.id) && !item.removed;
          };
          const hasChildren = (id) => {
            return id in scope.items;
          };

          const onClick = (e, item) => {
            scope.onSelect(item.id);
            scope.selectedId.id = item.id;
          };

          angular.extend(scope, {
            isSelected,
            hasChildren,
            onClick,
          });
        },
      });
    },
  };
}

export default ['treeItem', treeItem];
