import '../styles/app.css';

import appConfig from './config';

const app = angular.module('quantumTestApp', [
]);

app.config(appConfig);

// Providers
import storeDataLoader from './store/StoreDataLoader';
app.provider(...storeDataLoader);

// Controllers
import indexController from './controllers/index/index';
app.controller(...indexController);

// Factories
import Store from './store/Store';
app.factory(...Store);

import Cache from './store/Cache';
app.factory(...Cache);

// Directives
import RecursionHelper from './directives/RecursionHelper';
app.factory(...RecursionHelper);

import treeItem from './directives/treeItem/TreeItem';
app.directive(...treeItem);

import treeView from './directives/treeView/TreeView';
app.directive(...treeView);

export default app;
