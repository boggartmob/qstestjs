function appConfig($locationProvider,
                   StoreDataLoaderProvider,
                   $rootScopeProvider) {
  $locationProvider.html5Mode(false);

  StoreDataLoaderProvider.url = './store_items.json';
  StoreDataLoaderProvider.timeout = 600;
  StoreDataLoaderProvider.DEBUG = true;

  $rootScopeProvider.digestTtl(20);
}

export default appConfig;
