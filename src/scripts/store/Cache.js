import ChildrenCache from './ChildrenCache';

function Cache() {
  const self = {};
  const _childrenCache = new ChildrenCache();
  let _data = {};

  const _cb = () => {
  };

  let _updateCallback = _cb;

  self.setUpdateCallback = (cb = null) => {
    if (cb) {
      _updateCallback = cb;
    } else {
      _updateCallback = _cb;
    }
  };

  self.reset = () => {
    _data = {};
    _childrenCache.reset();
    _updateCallback(_data);
  };

  self.getData = () => {
    return _data;
  };

  self.getItemById = (id) => {
    return _data[id];
  };

  self.update = (items) => {
    Object.keys(items).map((key) => {
      _data[items[key].id] = items[key];
    });

    _updateCallback(_data);
  };

  self.deleteItem = (id) => {
    _childrenCache.getSubtreeIds(id).map((subtreeItemId) => {
      _data[subtreeItemId].remove();
    });

    _updateCallback(_data);
  };

  self.updateItem = (item) => {
    _data[item.id] = item;
    _updateCallback(_data);
  };

  self.createItem = (item) => {
    _data[item.id] = item;
    _childrenCache.update(item);

    _updateCallback(_data);
  };

  self.add = (item) => {
    if (_data[item.id]) {
      return;
    }

    _data[item.id] = item;
    _childrenCache.update(item);

    if (_data[item.parentId] && _data[item.parentId].removed) {
      _childrenCache.getSubtreeIds(item.parentId).map((subtreeItemId) => {
        _data[subtreeItemId].remove();
      });
    }

    _updateCallback(_data);
  };

  return self;
}

export default ['Cache', Cache];
