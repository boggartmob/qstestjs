class StoreDataLoader {

  constructor() {
    this.url = '/items.json';
    this.DEBUG = false;
  }


  $get($q, $http) {
    return {
      loadItems: () => {
        const deferred = $q.defer();

        if (this.DEBUG) {
          deferred.resolve(__TEST_DATA__);
        } else {
          $http.get(this.url)
            .success((data) => {
              deferred.resolve(data);
            })
            .error((e) => {
              throw new Error('Data loading failed. ' + e.message);
            });
        }

        return deferred.promise;
      },
    };
  }
}

export default ['StoreDataLoader', StoreDataLoader];
