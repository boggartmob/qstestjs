class ChildrenCache {
  constructor(items = []) {
    this._childrenCache = {};

    items.map((item) => {
      this.update(item);
    });
  }

  reset() {
    this._childrenCache = {};
  }

  update(item) {
    if (!this._childrenCache[item.parentId]) {
      this._childrenCache[item.parentId] = [];
    }

    if (this._childrenCache[item.parentId].indexOf(item.id) === -1) {
      this._childrenCache[item.parentId].push(item.id);
    }
  }

  getSubtreeIds(id, acc = []) {
    acc.push(id);

    if (this._childrenCache[id]) {
      this._childrenCache[id].map((itemId) => {
        acc.push(itemId);
        return this.getSubtreeIds(itemId, acc);
      });
    }

    return acc;
  }

  getChildren(id) {
    return this._childrenCache[id];
  }
}

export default ChildrenCache;
