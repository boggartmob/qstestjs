import StoreItem from './StoreItem';
import ChildrenCache from './ChildrenCache';

function Store(StoreDataLoader) {
  const self = {};
  let _data = {};
  const _childrenCache = new ChildrenCache();
  const _cb = () => {
  };

  let _updateCallback = _cb;

  function init() {
    _data = {};
    _childrenCache.reset();

    StoreDataLoader.loadItems().then((data) => {
      Object.keys(data).map((key) => {
        const model = new StoreItem(data[key]);
        self.setItem(model);
        _childrenCache.update(model);
      });

      _updateCallback(_data);
    });
  }

  self.setUpdateCallback = (cb = null) => {
    if (cb) {
      _updateCallback = cb;
    } else {
      _updateCallback = _cb;
    }
  };

  self.setItem = (item) => {
    _data[item.id] = item;
  };

  self.getData = () => {
    return _data;
  };

  self.getItemById = (id) => {
    return _data[id] || null;
  };

  self.reset = () => {
    init();
  };

  self.update = (items) => {
    const deletedItems = [];
    const updatedItems = {};

    Object.keys(items).map((key) => {
      const item = items[key].clone(); // to avoid update cache tree by link

      if (item.removed) {
        deletedItems.push(item);
      }

      _data[item.id] = item;
    });

    deletedItems.map((item) => {
      _childrenCache.getSubtreeIds(item.id).map((subtreeItemId) => {
        _data[subtreeItemId].remove();
      });
    });

    Object.keys(items).map((key) => {
      updatedItems[items[key].id] = _data[items[key].id].clone();
    });

    _updateCallback(_data);

    return updatedItems;
  };

  return self;
}

export default ['Store', Store];
