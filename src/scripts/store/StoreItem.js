class StoreItem {
  constructor(_rawData = null) {
    let structure = _rawData;

    if (!structure) {
      structure = StoreItem.getStructure();
    }

    Object.keys(structure).map((key) => {
      this[key] = structure[key];
    });
  }

  static getStructure() {
    return {
      id: Math.random().toString(),
      parentId: -1,
      removed: false,
      value: '',
    };
  }

  clone() {
    const _item = StoreItem.getStructure();
    _item.id = this.id;
    _item.parentId = this.parentId;
    _item.removed = this.removed;
    _item.value = this.value.toString();

    return new StoreItem(_item);
  }

  setParent(parentId) {
    if (!this.removed) {
      this.parentId = parentId;
    }
  }

  setValue(newValue) {
    if (!this.removed) {
      this.value = newValue;
    }
  }

  remove() {
    this.removed = true;
  }
}

export default StoreItem;
